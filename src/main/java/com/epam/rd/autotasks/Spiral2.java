package com.epam.rd.autotasks;

class Spiral2 {
    static int[][] spiral(int rows, int columns) {
        int number = 1;
        int[][] spiral = new int[rows][columns];

        for (int y = 0; y <columns; y++) {
            spiral[0][y] = number;
            number++;
        }
        for (int x = 1; x<rows; x++) {
            spiral[x][columns-1] = number;
            number++;
        }
        for (int y = columns - 2; y >= 0; y--) {
            spiral[rows - 1][y] = number;
            number++;
        }
        for (int x = rows - 2; x > 0; x--) {
            spiral[x][0] = number;
            number++;
        }
        int c = 1;
        int d = 1;

        while (number < rows * columns) {

            while (spiral[c][d + 1] == 0) {
                spiral[c][d] = number;
                number++;
                d++;
            }

            //Движемся вниз.
            while (spiral[c + 1][d] == 0) {
                spiral[c][d] = number;
                number++;
                c++;
            }

            //Движемся влево.
            while (spiral[c][d - 1] == 0) {
                spiral[c][d] = number;
                number++;
                d--;
            }

            //Движемся вверх.
            while (spiral[c - 1][d] == 0) {
                spiral[c][d] = number;
                number++;
                c--;
            }
        }
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                if (spiral[x][y] == 0) {
                    spiral[x][y] = number;
                }
            }
        }


        return spiral;
    }


    public static void main(String[] args) {

        int[][] spiral = Spiral2.spiral(1, 2);

        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral[i].length; j++) {
                System.out.print(String.format("%4s", spiral[i][j]));
            }
            System.out.println();
        }


    }


}

